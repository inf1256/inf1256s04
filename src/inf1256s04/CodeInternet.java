/**
 * @author Johnny Tsheke @ UQAM
 * 2017-01-30
 */
package inf1256s04;
import java.util.*;
public class CodeInternet {
      final static String CODE_FR="fr";
      final static String CODE_CA="ca";
      final static String CODE_BE="be";
	public static void main(String[] args) {
		// TODO Auto-generated method stub
       Scanner clavier = new Scanner(System.in);
       System.out.println("Entrez un code Internet à 2 lettres");
       String codeInt = clavier.next();
       switch(codeInt){
       case CODE_FR: System.out.println("France");
           break;
       case CODE_CA: System.out.println("Canada");
           break;
       case CODE_BE: System.out.println("Belgique");
           break;
       default: System.out.println("Je ne sais pas");
       }
	}

}
