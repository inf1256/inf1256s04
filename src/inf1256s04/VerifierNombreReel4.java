/**
 * @author Johnny Tsheke @ UQAM
 * 2017-01-30
 */
package inf1256s04;
import java.util.*;
public class VerifierNombreReel4 {
	public static void main(String[] args) {
		double nombreReelStrict;
		Scanner clavier = new Scanner(System.in);
		System.out.println("Entrez un nombre réel strict-- pas entier svp");
		try{
			try{
			if(clavier.hasNextInt()){
				//entier saisie, on génère une exception
				throw new Exception("Nombre entier saisi");
			}
			nombreReelStrict = clavier.nextDouble();
			}catch (Exception e) {
				System.out.println("ERREUR: "+e.getMessage());
				nombreReelStrict =clavier.nextDouble();
			}
			System.out.println("Le nombre réel obtenu est: "+nombreReelStrict);
		}catch (Exception e) {
			System.out.println("Ce n'est pas un nombre réel");
		}
		clavier.close();
	}
}
