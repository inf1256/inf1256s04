/**
 * @author Johnny Tsheke @ UQAM
 * 2017-01-30
 */
package inf1256s04;

public class AffichageFormatage {
    public static final double DIX = 10.0;
    public static final double TROIS = 3.0;
	public static void main(String[] args) {
		System.out.format("Afficher %2$.2f/%1$.2f = ",TROIS,DIX);
		System.out.format("%07.4f%n",DIX/TROIS);//avec retour à la ligne
		System.out.format("Avec formatage ");
    
	}

}
