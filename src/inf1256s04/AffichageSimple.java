/**
 * @author Johnny Tsheke @ UQAM
 * 2017-01-30
 */
package inf1256s04;

public class AffichageSimple {
    public static final double DIX = 10.0;
    public static final double TROIS = 3.0;
	public static void main(String[] args) {
		System.out.print("Afficher "+ DIX+"/"+TROIS+" = ");
		System.out.println(DIX/TROIS);
		System.out.println("Sans formatage ");
    
	}

}
